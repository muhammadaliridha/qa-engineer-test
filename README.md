# QA Engineer Test

This test is intended for prospective QA Engineer to join PT Lussa Teknologi Global. You need to do this test first before joining our team. Happy Coding and Good Luck!

## How To

- For QA Engineer we have coding test and QA skill test. The coding test is optional but recommended to take (we count both test score but with different weight)
- Fork this repositories into your own
- Your time spent to solve this problem are counted as your coding speed performance, faster you complete the test, then bigger the score
- You can write your solution using this tools https://playcode.io/new/ or you can use installed NodeJS on your local machine instead
- Follow the rules and answer the Tests below
- Email your result (forked repository URL) to **[web.jobs@lussa.net](mailto:web.jobs@lussa.net)** and **[jidni@lussa.net](mailto:jidni@lussa.net)** with following email subject `JSQA - [Full Name] - [WA Contact]`. Example : `JSQA - Lussa Alien - 0811 234 5678`

## Test 1

This test is a general coding test to check your algorithm skill

### Rules

- Write your solutions in test1 directory (create a new folder named `test1`)
- Solution files should be using this formats `<problem_number>.js`
- Write Readme (Markdown) for how to execute your code
- **No library allowed outside Javascript or ES Standard Package/Library**

### Problems 

1. Given a limited range of size `n` where array contains elements between `1` to `n-1` with one element repeating, find the duplicate number in it.

    **(5 points)**

    examples:
    ```javascript
    [1,2,3,4,4]
    the duplicate element is 4
    ```
    
    ```javascript
    [1,2,3,4,2]
    the duplicate element is 2
    ```

1. Given an array containing only `0`'s, `1`'s and `2`'s. sort the array in linear time and using constant space

    **(6 points)**

    examples:
    ```javascript
    [0,1,2,2,1,0,0,2,0,1,1,0]
    [0,0,0,0,0,1,1,1,1,2,2,2]
    ```

1. Given an array of integers find all distinct combinations of given length where repetition of elements is allowed

    **(6 points)**

    examples:
    ```javascript
    [1,2,3]
    [[1,1], [1,2], [1,3], [2,2], [2,3], [3,3]]
    ```

1. Given an array of integers, find maximum product (multiplication) subarray. In other words, find sub-array that has maximum product of its elements

    **(10 points)**

    examples:
    ```javascript
    [-6,4,-5,8,-10,0,8]
    the maximum product sub-array is [4,-5,8,-10] having product 1600
    ```

## Test 2

This test is a test to verify your QA skill

### Rules

- Write your solutions in test2 directory (create a new folder named `test2`)

### Problems 

- Go here : https://demoqa.com/
- Check in the left sidebar. On the last bottom there must be a menu called Book Store Application.
- You reverse engineer the flow (not so difficult like the real reverse engineering).
- By reverse engineer the flow, you gather the requirements from actual and working application (write into requirements document). Basically we are simulating the requirements gathering process.
- Write down the system flow (flowchart is ok or you can use any interaction diagram that you comfortable with) of that app.
- Make UI Mockup by taking screenshots of each feature of that app.
- Write the test plan based on the requirement document, system flow, and the UI mockup that you created.
- Write down the automation testing code (if you can do that)
- If you write down the automation testing, attach the video and the source code to the repository (so we can test it too).
- Do the manual testing based on your test plan and write down the result on test result document.
- Attach/Insert all of your document and automation testing code and video into your forked repository (folder `test2`)

## Questions?

Contact your employer for any question assistance
